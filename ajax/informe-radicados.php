<?php
session_start();

if(isset($_GET)){
    
    include_once '../global/configuracion.php';
    include_once RUTA_RAIZ.'model/Radicados.php';
    $Radicados = new Radicados();
    
    $Detalle = $Radicados->consultar(
        (($_GET["IdRadicado"] == "") ? NULL : $_GET["IdRadicado"]),
        (($_GET["UsuarioResponsable"] == "") ? NULL : $_GET["UsuarioResponsable"]),
        NULL,
        (($_GET["Departamento"] == "") ? NULL : $_GET["Departamento"])
    );
    $Lista = array();
    foreach ($Detalle as $Llave => $D) {
        $Lista[$Llave] = $D;
        $Lista[$Llave]["id_radicado"] = "E".$D['fecha'].str_pad($D["id_radicado"], 5, "0", STR_PAD_LEFT);
    }
    echo json_encode($Lista);
}