<?php
session_start();

if(isset($_GET) and $_GET["IdRadicado"] > 0){
    
    include_once '../global/configuracion.php';
    include_once RUTA_RAIZ.'model/Documentos_radicado.php';
    $DocRad = new Documentos_radicado();
    
    $Detalle = $DocRad->consultar(NULL, $_GET["IdRadicado"]);
    echo json_encode($Detalle);
}