<?php
include_once '../global/configuracion.php';
session_start();
include_once RUTA_RAIZ.'layout/header.php';

include_once RUTA_RAIZ.'model/Funcionarios.php';
$Funcionarios = new Funcionarios();
 
include_once RUTA_RAIZ.'model/Radicados.php';
$Radicados = new Radicados();

if(isset($_POST) and isset($_POST["Registrar"]) and $_POST["Registrar"] == "SI"){
    # Se crea traslado
    $Radicado = $Radicados->consultar($_POST["IdRadicado"]);
    if(count($Radicado) > 0 and $Radicado[0]["aceptado"] == "SI"){
        
        include_once RUTA_RAIZ.'model/Traslados.php';
        $Traslados = new Traslados();
        $Traslados->insertar($_POST["IdRadicado"], $_POST["UsuarioDestino"]);
        $Radicados->editarPorTraslado($_POST["IdRadicado"], "NO", $_POST["UsuarioDestino"], $_POST["Observacion"]);
        $Resultado = "Traslado creado exitosamente";
    }
    elseif (count($Radicado) == 0) {
        $Resultado = "El radicado a trasladar no existe";
    }
    else{
        $Resultado = "El radicado a trasladar no se encuentra aceptado";
    }
}
?>
<script src="../js/radicacion/trasladar.js"></script>
<script lang="javascript">
  if('<?php echo ((isset($Resultado)) ? $Resultado : "") ?>' != ""){
      alert('<?php echo ((isset($Resultado)) ? $Resultado : "")?>');
  }
</script>
<h1>Trasladar</h1>
<div class="col-xs-4">
  <form class="form-horizontal" action="" id="frmConsulta" name="frmConsulta" method="post">
    <legend>Formulario busqueda traslado</legend>
    <div class="form-group">
      <label class="col-xs-3" for="">Radicado #</label>
      <div class="col-xs-9">
        <select class="form-control" id="IdRadicadoConsulta">
          <?php
          $ListadoRadicados = $Radicados->consultar(NULL, $_SESSION["Usuario"]["id_funcionario"], "SI");
          foreach ($ListadoRadicados as $Radicado) {
            echo "<option value='".$Radicado["id_radicado"]."'>E".$Radicado['fecha'].str_pad($Radicado["id_radicado"], 5, "0", STR_PAD_LEFT)."</option>";
          }
          ?>
        </select>
      </div>
    </div>
    <button type="submit" class="btn btn-info btn-block">
      Consultar
    </button>
  </form>
</div>
<div class="col-xs-8" id="divDetalle" style="display: none !important;">
  <h2>Datos radicado</h2>
  <table class="table table-table-bordered table-hover">
    <thead>
      <tr>
        <th>Cantidad</th>
        <th>Documento</th>
      </tr>
    </thead>
    <tbody id="Detalle">

    </tbody>
  </table>
  <form class="form-horizontal" id="frmTrasladar" name="frmTrasladar" action="../radicacion/trasladar.php" method="post">
    <input type="hidden" name="Registrar" value="SI">
    <input type="hidden" name="IdRadicado" id="IdRadicadoTraslado" value="">
    <div class="form-group">
      <label class="col-xs-3" for="">Usuario</label>
      <div class="col-xs-9">
        <select class="form-control" name="UsuarioDestino" id="UsuarioDestino" required>
          <?php
          $ListadoFuncionarios = $Funcionarios->consultar();
          foreach ($ListadoFuncionarios as $Funcionario) {
            echo "<option value='".$Funcionario["id_funcionario"]."'>".$Funcionario["nombres"]." ".$Funcionario["apellidos"]."</option>";
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-xs-3" for="">Observación</label>
      <div class="col-xs-9">
        <input type="text" class="form-control QuitarEspeciales" name="Observacion" id="Observacion" value="" required />
      </div>
    </div>
    <button type="submit" class="btn btn-success">
      Trasladar
    </button>
  </form>
</div>
<?php
include_once RUTA_RAIZ.'layout/footer.php';