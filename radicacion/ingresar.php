<?php
include_once '../global/configuracion.php';
session_start();
$usr = $_SESSION['Usuario'];
include_once RUTA_RAIZ.'layout/header.php';

//include_once RUTA_RAIZ.'model/Radicados.php';
include_once RUTA_RAIZ.'model/Radicacion.php';
$Radicados = new Radicacion();
include_once RUTA_RAIZ.'model/Funcionarios.php';
$Funcionarios = new Funcionarios();
include_once RUTA_RAIZ.'model/mod_lugares.php';
$Lugares = new Lugares();
include_once RUTA_RAIZ.'model/TiposDocumentos.php';
$TiposDocumentos = new TiposDocumentos();

if(isset($_POST) and isset($_POST["Crear"]) and $_POST["Crear"] == "SI"){

    $Resultado = $Radicados->insertar($_POST['nomenvio'], $_POST['email'], $_POST['lgorigen'], $_POST['fcdestino'], $_POST['lgdestino'], $_POST['tpdoc'],  $_POST['cant'], $usr['id_funcionario']);
}
?>
<script lang="javascript">
  if('<?php echo ((isset($Resultado)) ? $Resultado : "") ?>' != ""){
      alert('<?php echo ((isset($Resultado)) ? $Resultado : "")?>');
  }
</script>
<h1>Radicaci&oacute;n</h1>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">Crear Radicado</div>
        <div class="panel-body">
            <form class="form-horizontal" action="../radicacion/ingresar.php" method="post">
               <div class="col-sm-6">
                    <input type="hidden" name="Crear" value="SI">
                    <div class="form-group">
                        <label class="col-xs-4" for="nomenvio">Nombre Quien Envia</label>
                        <div class="col-xs-8">
                            <input name="nomenvio" type="text" class="form-control" id="nomenvio" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4" for="email">Correo quien envia:</label>
                        <div class="col-xs-8">
                            <input name="email" type="email" class="form-control" id="email" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4" for="lgorigen">Lugar Origen</label>
                        <div class="col-xs-8">
                            <select name="lgorigen" id="lgorigen" class="form-control" required>
                              <option></option>
                              <?php
                              $ListadoLugaresOg = $Lugares->consultar();
                              foreach ($ListadoLugaresOg as $Lugarog) {
                                echo '<option value="'.$Lugarog["id_lugar"].'">'.$Lugarog["depto"].'  - '.$Lugarog["ciudad"].' - '.$Lugarog["lugar"].'</option>';
                              }
                              ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4" for="fcdestino">Funcionario Destino</label>
                        <div class="col-xs-8">
                            <select name="fcdestino" id="fcdestino" class="form-control" required>
                              <option></option>
                              <?php
                              $ListadoFuncionarios = $Funcionarios->consultar();
                              foreach ($ListadoFuncionarios as $Funcionario) {
                                echo '<option value="'.$Funcionario["id_funcionario"].'">'.$Funcionario["nombres"].'  '.$Funcionario["apellidos"].' / '.$Funcionario["lugar"].'</option>';
                              }
                              ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
<!--
                    <div class="form-group">
                        <label class="col-xs-4" for="lgdestino">Lugar Destino</label>
                        <div class="col-xs-8">
                            <select name="lgdestino" id="lgdestino" class="form-control" required>
                              <option></option>
                              <?php
//                              $ListadoLugares = $Lugares->consultar();
//                              foreach ($ListadoLugares as $Lugar) {
//                                echo '<option value="'.$Lugar["id_lugar"].'">'.$Lugar["depto"].'  - '.$Lugar["ciudad"].' - '.$Lugar["lugar"].'</option>';
//                              }
                              ?>
                            </select>
                        </div>
                    </div>
-->
                    <div class="form-group">
                        <label class="col-xs-4" for="tpdoc">Tipo Documento</label>
                        <div class="col-xs-8">
                            <select name="tpdoc" id="tpdoc" class="form-control" required>
                              <option></option>
                              <?php
                              $ListadoDocs = $TiposDocumentos->consultar();
                              foreach ($ListadoDocs as $doc) {
                                echo '<option value="'.$doc["id_documento"].'">'.$doc["descripcion"].'</option>';
                              }
                              ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4" for="cant">Cantidad:</label>
                        <div class="col-xs-8">
                            <input name="cant" type="number" class="form-control" id="cant" placeholder="" min="1" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 pull-right">
                            <button type="submit" class="btn btn-success">Crear</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-xs-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Radicado</th>
                        <th>Nombre Envia</th>
                        <th>Correo Envia</th>
                        <th>Aceptado</th>
                        <th>Radicador</th>
                        <th>Funcionario <br>Destino</th>
                        <th>Fecha Radicacion</th>
                        <th>Lugar Origen</th>
        <!--                <th>Lugar Destino</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $ListadoRadicados = $Radicados->consultar();
                        foreach ($ListadoRadicados as $Radicado) {
                            echo '<tr>
                            <td>E'.$Radicado['fecha'].str_pad($Radicado["id_radicado"], 5, "0", STR_PAD_LEFT).'</td>
                            <td>'.$Radicado["nombre_envia"].'</td>
                            <td>'.$Radicado["correo_envia"].'</td>
                            <td>'.$Radicado["aceptado"].'</td>
                            <td>'.$Radicado["radicador"].'</td>
                            <td>'.$Radicado["encargado"].'</td>
                            <td>'.$Radicado["fecha_radica"].'</td>
                            <td>'.$Radicado["origen"].'</td>
                            </tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

<?php
include_once RUTA_RAIZ.'layout/footer.php';
