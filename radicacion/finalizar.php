<?php
include_once '../global/configuracion.php';
session_start();
include_once RUTA_RAIZ.'layout/header.php';

include_once RUTA_RAIZ.'model/Radicados.php';
$Radicados = new Radicados();

if(isset($_POST) and isset($_POST["Finalizar"]) and $_POST["Finalizar"] == "SI"){
    # Se finaliza radicado
    $Radicado = $Radicados->consultar($_POST["IdRadicado"]);
    if(count($Radicado) > 0 and $Radicado[0]["aceptado"] == "SI"){

        $Radicados->editarPorAceptacion($_POST["IdRadicado"], "FINALIZADO", $_POST["Respuesta"]);
        $Resultado = "Radicado finalizado exitosamente";
        
        $Correo = "<h2>Cordial saludo</h2>
        <p>Su radicado del día (".$Radicado[0]["fecha_radica"].") a sido atendido:<br />
        Respuesta del funcionario que atiende: ".$_POST["Respuesta"]."
        </p>
        ";
        
        try{
            
            $Para      = $Radicado[0]["correo_envia"];
            $Titulo    = 'Radicado finalizado';
            $mensaje   = 'Hola';
            $Cabeceras = 'From: atencion@gestiondocumentalu.com' . "\r\n" .
                'Reply-To: atencion@gestiondocumentalu.com' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

            if(mail($Para, $Titulo, $Correo, $Cabeceras)){

            }
        } 
        catch (Exception $ex) {

        }
    }
    elseif(count($Radicado) == 0){
        $Resultado = "El radicado ingresado no existe";
    }
    else{
        $Resultado = "El radicado no se puede finalizar";
    }
}
?>
<script src="../js/radicacion/finalizar.js"></script>
<script lang="javascript">
  if('<?php echo ((isset($Resultado)) ? $Resultado : "") ?>' != ""){
      alert('<?php echo ((isset($Resultado)) ? $Resultado : "")?>');
  }
</script>
<h1>Finalizar</h1>
<div class="col-xs-4">
  <form class="form-horizontal" action="" id="frmConsulta" name="frmConsulta" method="post">
    <legend>Formulario de finalización</legend>
    <div class="form-group">
      <label class="col-xs-3" for="">Radicado #</label>
      <div class="col-xs-9">
        <select class="form-control" id="IdRadicadoConsulta">
          <?php
          $Listado = $Radicados->consultar(NULL, $_SESSION["Usuario"]["id_funcionario"], "SI");
          foreach ($Listado as $Radicado) {
            echo "<option value='".$Radicado["id_radicado"]."'>E".$Radicado['fecha'].str_pad($Radicado["id_radicado"], 5, "0", STR_PAD_LEFT)."</option>";
          }
          ?>
        </select>
      </div>
    </div>
    <button type="submit" class="btn btn-info btn-block">
      Consultar
    </button>
  </form>
</div>
<div class="col-xs-8" id="divDetalle" style="display: none !important;">
  <h2>Datos radicado</h2>
  <table class="table table-table-bordered table-hover">
    <thead>
      <tr>
        <th>Cantidad</th>
        <th>Documento</th>
      </tr>
    </thead>
    <tbody id="Detalle">

    </tbody>
  </table>
  <form class="form-horizontal" id="frmAceptar" name="frmAceptar" action="../radicacion/finalizar.php" method="post">
    <div class="form-group">
      <label for="" class="col-xs-3">Respuesta</label>
      <div class="col-xs-9">
        <input type="text" class="form-control" id="Respuesta" name="Respuesta" placeholder="" required>
      </div>
    </div>
    <input type="hidden" name="Finalizar" value="SI">
    <input type="hidden" name="IdRadicado" id="IdRadicadoFinalizar" value="">
    <button type="submit" class="btn btn-success">
      Finalizar
    </button>
  </form>
</div>
<?php
include_once RUTA_RAIZ.'layout/footer.php';
