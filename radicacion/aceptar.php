<?php
include_once '../global/configuracion.php';
session_start();
include_once RUTA_RAIZ.'layout/header.php';
 
include_once RUTA_RAIZ.'model/Radicados.php';
$Radicados = new Radicados();
 
if(isset($_POST) and isset($_POST["Registrar"]) and $_POST["Registrar"] == "SI"){
    # Se acepta asignación
    $Radicado = $Radicados->consultar($_POST["IdRadicado"]);
    if(count($Radicado) > 0 and $Radicado[0]["aceptado"] == "NO"){
        
        $Radicados->editarPorAceptacion($_POST["IdRadicado"], "SI");
        $Resultado = "Radicado aceptado exitosamente";
    }
    elseif(count($Radicado) == 0){
        $Resultado = "El radicado ingresado no existe";
    }
    else{
        $Resultado = "El radicado ya fue aceptado";
    }
}
?>
<script src="../js/radicacion/aceptar.js"></script>
<script lang="javascript">
  if('<?php echo ((isset($Resultado)) ? $Resultado : "") ?>' != ""){
      alert('<?php echo ((isset($Resultado)) ? $Resultado : "")?>');
  }
</script>
<h1>Aceptaciones</h1>
<div class="col-xs-4">
  <form class="form-horizontal" action="" id="frmConsulta" name="frmConsulta" method="post">
    <legend>Formulario de aceptaciones</legend>
    <div class="form-group">
      <label class="col-xs-3" for="">Radicado #</label>
      <div class="col-xs-9">
        <select class="form-control" id="IdRadicadoConsulta">
          <?php
          $Listado = $Radicados->consultar(NULL, $_SESSION["Usuario"]["id_funcionario"], "NO");
          print_r($Listado);
          foreach ($Listado as $Radicado) {
            echo "<option value='".$Radicado["id_radicado"]."'>E".$Radicado['fecha'].str_pad($Radicado["id_radicado"], 5, "0", STR_PAD_LEFT)."</option>";
          }
          ?>
        </select>
      </div>
    </div>
    <button type="submit" class="btn btn-info btn-block">
      Consultar
    </button>
  </form>
</div>
<div class="col-xs-8" id="divDetalle" style="display: none !important;">
  <h2>Datos radicado</h2>
  <table class="table table-table-bordered table-hover">
    <thead>
      <tr>
        <th>Cantidad</th>
        <th>Documento</th>
      </tr>
    </thead>
    <tbody id="Detalle">

    </tbody>
  </table>
  <form class="form-horizontal" id="frmAceptar" name="frmAceptar" action="../radicacion/aceptar.php" method="post">
    <input type="hidden" name="Registrar" value="SI">
    <input type="hidden" name="IdRadicado" id="IdRadicadoAceptar" value="">
    <button type="submit" class="btn btn-success">
      Aceptar
    </button>
  </form>
</div>
<?php
include_once RUTA_RAIZ.'layout/footer.php';