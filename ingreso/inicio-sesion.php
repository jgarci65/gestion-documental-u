<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(isset($_POST) and $_POST['Usuario'] != "" and $_POST['Contrasena'] != ""){

    include_once '../global/configuracion.php';
    include_once RUTA_RAIZ.'model/Funcionarios.php';
    $Funcionarios = new Funcionarios();

    $Usuario = $Funcionarios->consultar(NULL, $_POST['Usuario'], $_POST['Contrasena']);
    if(count($Usuario) > 0){

        session_start();
        $_SESSION['Usuario'] = $Usuario[0];
        header("location: ../index/bienvenido.php");
    }
    else{
        header("location: ../ingreso/index.php?Error=1");
    }
}
else
{
    header("location: ../ingreso/index.php");
}
