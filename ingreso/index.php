<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta charset="utf-8"> 
    <meta http-equiv="X-Frame-Options" content="deny"> 
    <meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate, private"> 
    <meta http-equiv="pragma" content="no-cache" /> 
    <meta name="author" content="Aplicativo - Javier Garcia Ruiz <javier.23.2010@gmail.com>"> 
    <meta name="author" content="Aplicativo - "> 
    <meta name="author" content="Aplicativo - ">
    <link rel="icon" href="../../favicon.ico">

    <title>Inicio sesión</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet"> 
    <link href="../css/navbar.css" rel="stylesheet"> 
    <link href="../css/grid.css" rel="stylesheet"> 
    <link href="../css/navbar-azul.css" rel="stylesheet"> 
    <link href="../css/sticky-footer.css" rel="stylesheet"> 
    <link href="../css/sign-in.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../js/ie-emulation-modes-warning.js"></script>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>
    .modal {
        overflow-y: auto;
    }
    .modal-open {
        overflow: auto;
    }
    </style>
  </head>

  <body>
    <!-- Fixed navbar -->
<nav class="navbar navbar-custom" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand" href="#"><img src="../img/MTInegativoverde.png" class="img-rounded" height="46px" /></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            
        </ul>
    </div>
  </div>
</nav>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body text-center">
              BIENVENIDO
            </div>
        </div>
    </div>
    
    <div class="container-fluid">
        <div class="container">
    <form class="form-signin" id="FormSignIn" class="form-horizontal" method="post" action="../ingreso/inicio-sesion.php" autocomplete="off">
        <fieldset>

            <h2 class="form-signin-heading">Favor, inicie sesión</h2>

            <div class="form-group">
                <label for="Usuario" class="sr-only">Usuario</label>
                <input type="text" id="Usuario" name="Usuario" class="form-control no-copy no-cut no-paste" placeholder="Usuario" style="text-transform:uppercase;" autofocus required />
                <label for="inputPassword" class="sr-only">Contraseña</label>
                <input type="password" id="Contrasena" name="Contrasena" class="form-control no-copy no-cut no-paste" placeholder="Contraseña" required>
            </div>
            
            <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
        </fieldset>
    </form>
</div>

    </div>

    <!-- MODAL MENSAJES ALERTA -->
    <div class="modal fade modal-alerta">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="modal-title">Mensaje de la página:</div>
                </div>
                <div class="modal-body" id="MensajeAlerta">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.2.min.js"></script> 
    <script src="../js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="../js/md5.js"></script> 
    <script src="../js/bootstrap.validator.min.js"></script> 
    <script src="../js/ingreso/sign-in.js"></script> 
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
