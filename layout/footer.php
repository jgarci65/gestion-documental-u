    </div>

    <!-- MODAL MENSAJES ALERTA -->
    <div class="modal fade modal-alerta">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="modal-title">Mensaje de la página:</div>
                </div>
                <div class="modal-body" id="MensajeAlerta">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <!--<footer class="footer">
      <div class="container">
        <p class="text-muted">&reg; Todos los derechos reservados. MTI 2018</p>
      </div>
    </footer>-->

    <!--<div id="divVisor" style="display: none; width:490px; height:700px;">
        <button id="visorAtras"><span style="padding: 0 !important;" class="mti-icon mti-visor-cerrar"></span>Cerrar Visor</button>
        <div id="resVisor"></div>
    </div>-->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="../js/jquery-1.11.2.min.js"></script> 
    <script src="../js/bootstrap.min.js"></script> --> 
    <script type="text/javascript" src="../js/bootstrap-waitingfor.js"></script> 
    <script type="text/javascript" src="../js/Moment.js"></script> 
    <script type="text/javascript" src="../js/bootstrap-datetimepicker.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
