<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-Frame-Options" content="deny">
    <meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate, private">
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="author" content="Aplicativo - Javier Garcia Ruiz <javier.23.2010@gmail.com>">
    <meta name="author" content="Aplicativo - ">
    <meta name="author" content="Aplicativo - ">
    <link rel="icon" href="../../favicon.ico">

    <title>Gestión documental</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/navbar.css" rel="stylesheet">
    <link href="../css/grid.css" rel="stylesheet">
    <link href="../css/navbar-azul.css" rel="stylesheet">
    <link href="../css/sticky-footer.css" rel="stylesheet">
    <link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../js/ie-emulation-modes-warning.js"></script>
    <script src="../js/jquery-1.11.2.min.js"></script> 
    <script src="../js/bootstrap.min.js"></script> 


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
    .modal {
        overflow-y: auto;
    }
    .modal-open {
        overflow: auto;
    }
    </style>
  </head>

  <body>
    <!-- Fixed navbar -->
<nav class="navbar navbar-custom" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand" href="#"><img src="../img/MTInegativoverde.png" class="img-rounded" height="46px" /></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <?php
            if($_SESSION['Usuario']['perfil'] == "ADMINISTRADOR" or $_SESSION['Usuario']['perfil'] == "RADICADOR"){
                ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Radicacion <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu"><li><a href="../radicacion/ingresar.php">Ingresar</a></li>
                        <li><a href="../radicacion/aceptar.php">Aceptaciones</a></li>
                        <li><a href="../radicacion/trasladar.php">Trasladar</a></li>
                        <li><a href="../radicacion/finalizar.php">Finalizar</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Informes <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu"><li><a href="../informes/radicados.php">Radicados</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Administración <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../administracion/deptos_x_pais.php">Departamentos</a></li>
                        <li><a href="../administracion/ciudades.php">Ciudades</a></li>
                        <li><a href="../administracion/lugares.php">Lugares</a></li>
                        <?php
                        if($_SESSION['Usuario']['perfil'] == "ADMINISTRADOR"){
                            ?>
                        <li><a href="../administracion/usuarios.php">Usuarios</a></li>
                        <li><a href="../administracion/departamentos.php">Areas Organizacion</a></li>
                        <li><a href="../administracion/tipos-documentos.php">Tipos de documentos</a></li>
                        <li><a href="../administracion/paises.php">Paises</a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </li>
                <?php
            }
                ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Sesion <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu"><li><a href="../salida/salir.php">Salir</a></li>
                    </ul>
                </li>
        </ul>
    </div>
  </div>
</nav>
    <div class="container"> 
        <div class="panel-body text-center">
            <?php
            if(isset($_SESSION['Usuario'])){
                echo "USURIO EN SESIÓN: ".$_SESSION['Usuario']['nombres']." ".$_SESSION['Usuario']['apellidos'];
            }
            else{
                echo "BIENVENIDO";
            }
            ?>
        </div>