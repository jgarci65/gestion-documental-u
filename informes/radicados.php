<?php
include_once '../global/configuracion.php';
session_start();
include_once RUTA_RAIZ.'layout/header.php';

include_once RUTA_RAIZ.'model/Funcionarios.php';
$Funcionarios = new Funcionarios();

include_once RUTA_RAIZ.'model/Departamentos.php';
$Departamentos = new Departamentos();

?>
<script src="../js/informes/radicados.js"></script>
<div class="row">
  <h1>Informe radicados</h1>
  <div class="col-xs-4">
    <form class="form-horizontal" action="" name="frmInforme" id="frmInforme" method="get">
      <div class="form-group">
        <label class="col-xs-4" for=""># Radicado</label>
        <div class="col-xs-8">
          <input class="form-control" type="number" min="1" name="IdRadicado" id="IdRadicado" value="">
        </div>
      </div>
      <div class="form-group">
        <label class="col-xs-4" for="">Usuario responsable</label>
        <div class="col-xs-8">
          <select class="form-control" name="UsuarioResponsable" id="UsuarioResponsable">
              <option value="">TODOS</option>
              <?php
              $ListadoFuncionarios = $Funcionarios->consultar();
              foreach ($ListadoFuncionarios as $Funcionario) {
                  echo "<option value='".$Funcionario["id_funcionario"]."'>".$Funcionario["nombres"]." ".$Funcionario["apellidos"]."</option>";
              }
              ?>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-xs-4" for="">Departamento</label>
        <div class="col-xs-8">
          <select class="form-control" name="Departamento" id="Departamento">
            <option value="">TODOS</option>
              <?php
              $ListadoDepartamentos = $Departamentos->consultar();
              foreach ($ListadoDepartamentos as $Departamento) {
                 echo "<option value='".$Departamento["id_departamento"]."'>".$Departamento["descripcion"]."</option>";
              }
              ?>
          </select>
        </div>
      </div>
      <div class="col-xs-12">
        <span class="alert alert-info">Puede utilizar uno o varios filtros a la vez</span>
      </div>
      <button type="submit" class="btn btn-info">
        Consultar
      </button>
    </form>
  </div>
  <div class="col-xs-8">
    <table class="table table-border table-hover">
      <thead>
        <tr>
          <th>#</th>
          <th>Envía</th>
          <th>Correo envía</th>
          <th>Responsable</th>
          <th>Aceptado</th>
          <th>Departamento</th>
        </tr>
      </thead>
      <tbody id="Detalle">

      </tbody>
    </table>
  </div>
</div>
