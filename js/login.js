$(document).ready(function (){
//    $.ajaxLoadingModal();
    
    $("#recuperarContrasena").click(function (){
        $para='';
        $para+='&appNombre='+$('#appNombre').val();
        $para+='&appCliente='+$('#appCliente').val();
        $para+='&ingresarPuk='+'true';
        $.post('../../admon/user/setpassword.php',$para,function (data){
            $("#tabs-1").html(data);
        });
    });
    //MD5
    $("#cancelarPass").click(function (){
        window.location='index.php';
    });
    
    $('#puk').keyup(function () {
        if(this.value != this.value.replace(/[^0-9a-zA-Z]/g,'')){
            alert($('#novalido').val());            
            this.value = this.value.replace(/[^0-9a-zA-Z]/g,'');
        }          
        if(this.value.length >= '30'){            
            alert($('#mayorvalor').val());              
            window.location ="../login/index.php";   
        }
    });
    
    $("#epuk").click(function (){
        $para='';
        $para+='&appCliente='+$('#appCliente').val();
        $para+='&ingresarPuk='+'false';
        $para+='&puk='+$('#puk').val();
        $para+='&enviarPuk='+'true';
        $.post('../../admon/user/setpassword.php',$para,function (data){
            $("#tabs-1").html(data);
        });
    });
    
    $("#login").blur(function (){
        $(this).val($(this).val().toUpperCase());
    });
    
    $("#password").removeAttr("disabled").val('');
    $("#passwordM").val('');
    
    $("#loger").click(function (){
        if(valLog()){
            $("#passwordM").val(MD5($("#password").val()));
            $("#password").attr("disabled","disabled");
            $("#floger").submit();
        }
    });
    $("#password").keyup(function (event){
        if (event.keyCode == 13) {
            if(valLog()){
                $("#passwordM").val(MD5($("#password").val()));
                $("#password").attr("disabled","disabled");
                $("#floger").submit();
            }
        }
    });
    if($("#sufijo").length!=0)
        if($("#sufijo").val()=='')
            $("#sufijoApp").val($("#sufijo").val());

});

function valLog(){
    r=true;
    e=true;
    m='';
    $("#login, #password, #newpassword, #renewpassword").removeClass('ui-state-error');
    if($("#login").val().toString().length==0){
        m="Ingresa el Login\n";
        r=false;
        $("#login").addClass('ui-state-error');
    }
    $n=/^\d+$/;
    if(!$n.test($("#password").val()))
        $.each(er,function (k,v){
            exp='/'+v.expreg+'/';
            exp=eval(exp);
            $n=(v.must=='F')?'':'!';
            c=$n+'exp.test($("#password").val())';
            if(eval(c)){
                e=false;
                m+=v.message+'\n';
            }
        });
    if(!e){
        r=false;
        $("#password").addClass('ui-state-error');
    }
    if(!r)
        alert(m);
    return r;
}