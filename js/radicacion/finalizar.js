$(function(){

  $("#frmConsulta").on('submit', function(event) {
    event.preventDefault();
    /* Act on the event */
    $.ajax({
      url: '../ajax/consultar-radicado.php',
      type: 'GET',
      dataType: 'json',
      data: {
        IdRadicado: $("#IdRadicadoConsulta").val()
      }
    })
    .done(function(Json) {

      var Html = "";
      $.each(Json, function(index, el) {
        Html += '<tr>'
           + '<td>'+el["cantidad"]+'</td>'
           + '<td>'+el["documento"]+'</td>'
         + '</tr>';
      });
      $("#Detalle").html(Html);
      $("#divDetalle").css("display", "inline");

      $("#IdRadicadoFinalizar").val($("#IdRadicadoConsulta").val());
    })
    .fail(function() {

    })
    .always(function() {

    });

  });
});
