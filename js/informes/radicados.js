$(function(){

  $("#frmInforme").on('submit', function(event) {
    event.preventDefault();
    /* Act on the event */
    $.ajax({
      url: '../ajax/informe-radicados.php',
      type: 'GET',
      dataType: 'json',
      data: $("#frmInforme").serialize()
    })
    .done(function(Json) {

      if($(Json).size() > 0){
        Html = "";
        $.each(Json, function(index, el) {
          Html = '<tr>'
             + '<td>'+el['id_radicado']+'</td>'
             + '<td>'+el['nombre_envia']+'</td>'
             + '<td>'+el['correo_envia']+'</td>'
             + '<td>'+el['nombres']+" "+el['apellidos']+'</td>'
             + '<td>'+el['aceptado']+'</td>'
             + '<td>'+el['departamento']+'</td>'
           + '</tr>';
        });
        $("#Detalle").html(Html);
      }
    })
    .fail(function() {

    })
    .always(function() {

    });

  });
});
