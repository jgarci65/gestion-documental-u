var Tam = null;
$(document).ready(function (){
    
    /*tamaño ventana*/
    Tam = TamVentana();
    $("#biw").val(Tam[0]);
    $("#bih").val(Tam[1]);

    window.onresize = function() {
        Tam = TamVentana();
        $("#biw").val(Tam[0]);
        $("#bih").val(Tam[1]);
    };
});

function TamVentana() {
    var Tamanyo = [0, 0];
    if (typeof window.innerWidth != 'undefined')
    {
        Tamanyo = [
        window.innerWidth,
        window.innerHeight
        ];
    }
    else if (typeof document.documentElement != 'undefined'
        && typeof document.documentElement.clientWidth !=
        'undefined' && document.documentElement.clientWidth != 0)
        {
        Tamanyo = [
        document.documentElement.clientWidth,
        document.documentElement.clientHeight
        ];
    }
    else   {
        Tamanyo = [
        document.getElementsByTagName('body')[0].clientWidth,
        document.getElementsByTagName('body')[0].clientHeight
        ];
    }
    return Tamanyo;
}