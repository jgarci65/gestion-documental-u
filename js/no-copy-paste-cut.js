$(function(){
   
    $(".no-copy").bind('copy', function(e) {
        e.preventDefault();
        
        $("#MensajeAlerta").html("La función copiar (Control+C) esta deshabilitada en este campo.");
        $(".modal-alerta").modal("show");
    });
    
    $(".no-paste").bind('paste', function(e) {
        e.preventDefault();
        
        $("#MensajeAlerta").html("La función pegar (Control+V) esta deshabilitada en este campo.");
        $(".modal-alerta").modal("show");
    });
    
    $(".no-cut").bind('cut', function(e) {
        e.preventDefault();
        
        $("#MensajeAlerta").html("La función copiar cortar(Control+X) esta deshabilitada en este campo.");
        $(".modal-alerta").modal("show");
    });
});