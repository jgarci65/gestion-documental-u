<?php
include_once RUTA_RAIZ.'model/Conexion.php';
/**
 * Description of Funcionarios
 *
 * @author javierg.garcia
 */
class Perfiles extends Conexion {
    
    public function __construct() {
        
        parent::__construct();
    }
    
    public function consultar($Id = NULL) {
        
        $Filtro = array();
        if(!is_null($Id)) $Filtro[] = "p.id_perfil = ".$Id;
        
        $Sql = "
        SELECT p.*
        FROM perfiles p
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }
}