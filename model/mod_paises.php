<?php
include_once RUTA_RAIZ.'model/Conexion.php';
/**
 * Description of Funcionarios
 *
 * @author javierg.garcia
 */
class Paises extends Conexion
{

    public function __construct() {

        parent::__construct();
    }

    public function consultar($Id = NULL) {

        $Filtro = array();
        if(!is_null($Id)) $Filtro[] = "d.id_pais = ".$Id;

        $Sql = "
        SELECT d.*
        FROM paises d
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }

    public function verificar($Pais = NULL) {

        $Filtro = array();
        if(!is_null($Pais)) $Filtro[] = "lower(d.descripcion) = '".strtolower($Pais)."'";

        $Sql = "
        SELECT d.*
        FROM paises d
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }

    public function insertar($Pais) {

        if($Pais != "")
        {
            $Sql = "
            INSERT INTO paises (
                descripcion
            )
            VALUES (
                '".strtoupper($Pais)."'
            )
            ";
            $this->noquery($Sql);
            return "Creado exitosamente.";
        }
        else{
            return "Faltan datos obligatorios.";
        }
    }
}
