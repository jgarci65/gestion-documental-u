<?php
include_once RUTA_RAIZ.'model/Conexion.php';
/**
 * Description of Funcionarios
 *
 * @author javierg.garcia
 */
class Funcionarios extends Conexion {
    
    public function __construct() {
        
        parent::__construct();
    }
    
    public function consultar($Id = NULL, $Usuario = NULL, $Contrasena = NULL) {
        
        $Filtro = array();
        if(!is_null($Id)) $Filtro[] = "f.id_funcionario = ".$Id;
        if(!is_null($Usuario)) $Filtro[] = "f.usuario = '".$Usuario."'";
        if(!is_null($Contrasena)) $Filtro[] = "f.contrasena = '".$Contrasena."'";
        
        $Sql = "
        SELECT f.*, p.descripcion AS perfil, d.descripcion AS departamento, CONCAT(lg.pais,' - ',lg.depto,' - ',lg.ciudad,' - ',lg.lugar) as lugar
        FROM funcionarios f
            LEFT JOIN perfiles p ON f.id_perfil = p.id_perfil
            LEFT JOIN departamentos d ON f.id_departamento = d.id_departamento
            LEFT JOIN v_lugares lg ON f.id_lugar = lg.id_lugar
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }
    
    public function insertar($Nombres, $Apellidos, $Usuario, $Contrasena, $Perfil, $Departamento, $Lugar) {
        
        if($Nombres != "" and $Apellidos != "" and $Usuario != "" and $Contrasena != "" and $Perfil != "" and $Departamento != "" and $Lugar != ""){
            
            $Sql = "
            INSERT INTO funcionarios (
                nombres, apellidos, usuario, contrasena, id_perfil, id_departamento, id_lugar
            )
            VALUES (
                '".$Nombres."', '".$Apellidos."', '".$Usuario."', '".md5($Contrasena)."', ".$Perfil.", ".$Departamento." , ".$Lugar."
            )
            ";
            $this->noquery($Sql);
            return "Creado exitosamente.";
        }
        else{
            return "Faltan datos obligatorios.";
        }
    }
}
