<?php
include_once RUTA_RAIZ.'model/Conexion.php';
/**
 * Description of Documentos_radicados
 *
 * @author javierg.garcia
 */
class Documentos_radicado extends Conexion{
    
    public function __construct() {
        
        parent::__construct();
    }
    
    public function consultar($IdDocRad = NULL, $IdRadicado = NULL) {
        
        $Filtro = array();
        if(!is_null($IdDocRad)) $Filtro[] = "dr.id_documentos_radicado = ".$IdDocRad;
        if(!is_null($IdRadicado)) $Filtro[] = "dr.id_radicado = ".$IdRadicado;
        
        $Sql = "
        SELECT dr.*, r.*, d.descripcion AS documento
        FROM documentos_radicado dr
            INNER JOIN radicados r ON dr.id_radicado = r.id_radicado
            LEFT JOIN documentos d ON dr.id_documento = d.id_documento
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }
}