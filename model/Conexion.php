<?php
/**
 * Description of Conexion
 *
 * @author javierg.garcia
 */
class Conexion
{

    private $Link;

    public function __construct() {

        $this->Link = mysqli_connect('localhost', 'dlloemp', 'Dlloemp3063') or die('No se pudo conectar: ' . mysql_error());
        mysqli_select_db($this->Link, 'dlloemp') or die('No se pudo seleccionar la base de datos');
    }

    public function query($Query) {

        $Resultado = mysqli_query($this->Link, $Query) or die('Consulta fallida: ' . mysqli_error($this->Link));
        $Devolver = array();
        while($Linea = mysqli_fetch_assoc($Resultado)){
            $Devolver[] = $Linea;
        }
        return $Devolver;
    }

    public function noquery($Query) {

        mysqli_query($this->Link, $Query) or die('Consulta fallida: <br />'. mysqli_error($this->Link));
    }

    public function __destruct() {

        mysqli_close($this->Link);
    }
}
