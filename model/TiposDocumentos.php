<?php
include_once RUTA_RAIZ.'model/Conexion.php';
/**
 * Description of Funcionarios
 *
 * @author lcasallas
 */
class TiposDocumentos extends Conexion
{

    public function __construct() {

        parent::__construct();
    }

    public function consultar($Id = NULL) {

        $Filtro = array();
        if(!is_null($Id)) $Filtro[] = "d.id_documento = ".$Id;

        $Sql = "
        SELECT d.*,CONCAT(f.nombres,' ',f.apellidos) as usuario
        FROM documentos d
        JOIN funcionarios f ON d.id_funcionario_registra = id_funcionario
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }

    public function verificar($Documento = NULL) {

        $Filtro = array();
        if(!is_null($Documento)) $Filtro[] = "lower(d.descripcion) = '".strtolower($Documento)."'";

        $Sql = "
        SELECT d.*
        FROM documentos d
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }

    public function insertar($Documento,$usr) {

        if($Documento != "" && $usr != "")
        {
            $Sql = "
            INSERT INTO documentos (
                descripcion
                ,id_funcionario_registra
                ,fecha_registro
            )
            VALUES (
                '".strtoupper($Documento)."'
                ,".$usr."
                ,NOW()
            )
            ";
            $this->noquery($Sql);
            return "Creado exitosamente.";
        }
        else{
            return "Faltan datos obligatorios.";
        }
    }
}
