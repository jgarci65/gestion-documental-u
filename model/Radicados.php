<?php
include_once RUTA_RAIZ.'model/Conexion.php';
/**
 * Description of Radicados
 *
 * @author javierg.garcia
 */
class Radicados extends Conexion{

    public function __construct() {

        parent::__construct();
    }

    public function consultar($Id = NULL, $IdResponsable = NULL, $Aceptado = NULL, $Departamento = NULL) {

        $Filtro = array();
        if(!is_null($Id)) $Filtro[] = "r.id_radicado = ".$Id;
        if(!is_null($IdResponsable)) $Filtro[] = "r.id_funcionario_responsable = ".$IdResponsable;
        if(!is_null($Aceptado)) $Filtro[] = "r.aceptado = '".$Aceptado."'";
        if(!is_null($Departamento)) $Filtro[] = "r.id_departamento = '".$Departamento."'";

        $Sql = '
        SELECT r.*, d.descripcion AS departamento, f.nombres, f.apellidos, DATE_FORMAT(fecha_radica, "%Y%m%e") AS fecha
        FROM radicados r
            LEFT JOIN departamentos d ON r.id_departamento = d.id_departamento
            LEFT JOIN funcionarios f ON r.id_funcionario_responsable = f.id_funcionario
        '.((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }

    public function editarPorTraslado($IdRadicado, $Aceptado, $IdResponsable, $Observacion = "") {

        $Sql = "
        UPDATE radicados SET id_funcionario_responsable = ".$IdResponsable.", aceptado = '".$Aceptado."', observacion = '".$Observacion."'
        WHERE id_radicado = ".$IdRadicado."
        ";
        $this->noquery($Sql);
    }

    public function editarPorAceptacion($IdRadicado, $Aceptado, $Mensaje = "") {

        $Sql = "
        UPDATE radicados SET aceptado = '".$Aceptado."' ".(($Mensaje != "") ? ", respuesta = '$Mensaje'" : "")."
        WHERE id_radicado = ".$IdRadicado."
        ";
        $this->noquery($Sql);
    }
}
