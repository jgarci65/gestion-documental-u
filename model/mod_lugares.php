<?php
include_once RUTA_RAIZ.'model/Conexion.php';
/**
 * Description of Funcionarios
 *
 * @author javierg.garcia
 */
class Lugares extends Conexion {

    public function __construct() {

        parent::__construct();
    }

    public function consultar($Lugar = NULL) {

        $Filtro = array();
        if(!is_null($Lugar)) $Filtro[] = "lower(cd.descripcion) = '".strtolower($Lugar)."'";

        $Sql = "
        SELECT lg.id as id_lugar,upper(p.descripcion) AS pais, upper(dp.descripcion) as depto, upper(cd.descripcion) as ciudad, upper(lg.descripcion) as lugar
        FROM lugares lg
        JOIN ciudades cd ON lg.id_ciudad = cd.id
        JOIN departamentos_pais dp ON cd.id_depto = dp.id
        JOIN paises p ON dp.id_pais = p.id
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }

    public function verificar($idciudad = NULL, $lugar = NULL) {

        $Filtro = array();
        if(!is_null($idciudad)) $Filtro[] = "id_depto = '".$idciudad."'";
        if(!is_null($lugar)) $Filtro[] = "lower(d.descripcion) = '".strtolower($lugar)."'";

        $Sql = "
        SELECT d.*
        FROM lugares d
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }

    public function insertar($id_ciudad, $Lugar) {

        if($id_ciudad != "" and $Lugar != ""){

            $Sql = "
            INSERT INTO lugares (
                id_ciudad, descripcion
            )
            VALUES (
                ".$id_ciudad.", '".$Lugar."'
            )
            ";
            $this->noquery($Sql);
            return "Creado exitosamente.";
        }
        else{
            return "Faltan datos obligatorios.";
        }
    }
}
