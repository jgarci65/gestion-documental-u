<?php
include_once RUTA_RAIZ.'/model/Conexion.php';

class Radicacion extends Conexion {

    public function __construct() {

        parent::__construct();
    }

    public function consultar() {
        $Sql = '
                SELECT a.id_radicado
                        ,a.nombre_envia
                        ,a.correo_envia,a.aceptado, a.fecha_radica ,CONCAT(b.nombres," ",b.apellidos) as encargado
                        , CONCAT(c.nombres," ",c.apellidos) as radicador
                        ,CONCAT(d.depto," ",d.ciudad," ",d.lugar) as origen
                        ,DATE_FORMAT(fecha_radica, "%Y%m%e") AS fecha
                FROM radicados a
                join funcionarios b on a.id_funcionario_responsable = b.id_funcionario
                join funcionarios c on a.id_funcionario_radica = c.id_funcionario
                join v_lugares d on a.id_lugar_origen = d.id_lugar
                ORDER BY fecha_radica DESC
                ';
        return $this->query($Sql);
    }

    public function insertar($nomenvia, $emailenvia, $lgorigen, $fcdestino, $lgdestino, $tpdoc, $cant, $usr)
    {
        if($nomenvia != "" and $lgorigen != "" and $fcdestino != "" and $tpdoc != "" and $cant != "" and $usr != "")
        {
            $Sql = "
            INSERT INTO radicados (
                	nombre_envia,correo_envia, id_funcionario_responsable,aceptado, id_funcionario_radica, id_lugar_origen, fecha_radica
            )
            VALUES (
                '".$nomenvia."','".$emailenvia."',".$fcdestino.",'NO',".$usr.",".$lgorigen.", NOW()
            )";

            $this->noquery($Sql);

            $Sql2 = "
                    SELECT max(id_radicado) as id
                    FROM radicados
                    ";

            $id_radicado = $this->query($Sql2);
            foreach ($id_radicado as $rad) {
                $id_r = $rad['id'];
            }

            $Sql3 = "
            INSERT INTO documentos_radicado (id_radicado, id_documento, cantidad, id_funcionario_registra, fecha_registro)
            VALUES
            (".$id_r.",".$tpdoc.",".$cant.",".$usr.", NOW())";

            $this->noquery($Sql3);

            return "Creado exitosamente.";
        }
        else{
            return "Faltan datos obligatorios.";
        }
    }
}
