<?php
include_once RUTA_RAIZ.'model/Conexion.php';
/**
 * Description of Funcionarios
 *
 * @author javierg.garcia
 */
class Ciudades extends Conexion {

    public function __construct() {

        parent::__construct();
    }

    public function consultar($Ciudad = NULL) {

        $Filtro = array();
        if(!is_null($Ciudad)) $Filtro[] = "lower(cd.descripcion) = '".strtolower($Ciudad)."'";

        $Sql = "
        SELECT dp.id as id_ciudad,upper(p.descripcion) AS pais, upper(dp.descripcion) as depto, upper(cd.descripcion) as ciudad
        FROM ciudades cd
        JOIN departamentos_pais dp ON cd.id_depto = dp.id
        JOIN paises p ON dp.id_pais = p.id
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }

    public function verificar($Depto = NULL, $Ciudad = NULL) {

        $Filtro = array();
        if(!is_null($Depto)) $Filtro[] = "id_depto = '".$Depto."'";
        if(!is_null($Ciudad)) $Filtro[] = "lower(d.descripcion) = '".strtolower($Ciudad)."'";

        $Sql = "
        SELECT d.*
        FROM ciudades d
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }

    public function insertar($id_depto, $Ciudad) {

        if($id_depto != "" and $Ciudad != ""){

            $Sql = "
            INSERT INTO ciudades (
                id_depto, descripcion
            )
            VALUES (
                ".$id_depto.", '".$Ciudad."'
            )
            ";
            $this->noquery($Sql);
            return "Creado exitosamente.";
        }
        else{
            return "Faltan datos obligatorios.";
        }
    }
}
