<?php
include_once RUTA_RAIZ.'model/Conexion.php';
/**
 * Description of Traslados
 *
 * @author javierg.garcia
 */
class Traslados extends Conexion{
    
    public function __construct() {
        
        parent::__construct();
    }
    
    public function consultar($Id = NULL, $IdfuncionarioDestino = NULL) {
        
        $Filtro = array();
        if(!is_null($Id)) $Filtro[] = "t.id_traslado = ".$Id;
        if(!is_null($IdfuncionarioDestino)) $Filtro[] = "t.id_funcionario_destino = ".$IdfuncionarioDestino;
        
        $Sql = "
        SELECT t.*
        FROM traslados t
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }
    
    public function insertar($IdRadicado, $UsuarioDestino) {
        
        $Sql = "
        INSERT INTO traslados (
            id_radicado, id_funcionario_destino, id_funcionario_origen, fecha_registro
        )
        VALUES (
            ".$IdRadicado.", ".$UsuarioDestino.", ".$_SESSION["Usuario"]["id_funcionario"].", NOW()
        )
        ";
        $this->noquery($Sql);
    }
}