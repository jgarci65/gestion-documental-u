<?php
include_once RUTA_RAIZ.'model/Conexion.php';
/**
 * Description of Funcionarios
 *
 * @author javierg.garcia
 */
class DeptoPais extends Conexion {

    public function __construct() {

        parent::__construct();
    }

    public function consultar($Depto = NULL) {

        $Filtro = array();
        if(!is_null($Depto)) $Filtro[] = "lower(dp.descripcion) = '".strtolower($Depto)."'";

        $Sql = "
        SELECT dp.id as id_depto, dp.descripcion as depto, p.descripcion AS pais
        FROM departamentos_pais dp
        JOIN paises p ON dp.id_pais = p.id
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }

    public function verificar($Pais = NULL, $Depto = NULL) {

        $Filtro = array();
        if(!is_null($Pais)) $Filtro[] = "id_pais = '".$Pais."'";
        if(!is_null($Depto)) $Filtro[] = "lower(d.descripcion) = '".strtolower($Depto)."'";

        $Sql = "
        SELECT d.*
        FROM departamentos_pais d
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }

    public function insertar($id_pais, $Depto) {

        if($id_pais != "" and $Depto != ""){

            $Sql = "
            INSERT INTO departamentos_pais (
                id_pais, descripcion
            )
            VALUES (
                ".$id_pais.", '".$Depto."'
            )
            ";
            $this->noquery($Sql);
            return "Creado exitosamente.";
        }
        else{
            return "Faltan datos obligatorios.";
        }
    }
}
