<?php
include_once RUTA_RAIZ.'model/Conexion.php';
/**
 * Description of Funcionarios
 *
 * @author javierg.garcia
 */
class Departamentos extends Conexion
{

    public function __construct() {

        parent::__construct();
    }

    public function consultar($Id = NULL) {

        $Filtro = array();
        if(!is_null($Id)) $Filtro[] = "d.id_departamento = ".$Id;

        $Sql = "
        SELECT d.*
        FROM departamentos d
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }

    public function verificar($Departamento = NULL) {

        $Filtro = array();
        if(!is_null($Departamento)) $Filtro[] = "lower(d.descripcion) = '".strtolower($Departamento)."'";

        $Sql = "
        SELECT d.*
        FROM departamentos d
        ".((count($Filtro) > 0) ? 'WHERE '.implode(" AND ",$Filtro) : '')."
        ";
        return $this->query($Sql);
    }

    public function insertar($Departamento) {

        if($Departamento != "")
        {
            $Sql = "
            INSERT INTO departamentos (
                descripcion
            )
            VALUES (
                '".strtoupper($Departamento)."'
            )
            ";
            $this->noquery($Sql);
            return "Creado exitosamente.";
        }
        else{
            return "Faltan datos obligatorios.";
        }
    }
}
