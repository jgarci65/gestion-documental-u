<?php
include_once '../global/configuracion.php';
session_start();
include_once RUTA_RAIZ.'layout/header.php';

include_once RUTA_RAIZ.'model/Departamentos.php';
$Departamentos = new Departamentos();

if(isset($_POST) and isset($_POST["Crear"]) and $_POST["Crear"] == "SI"){
    # Se crea nuevo usuario
    $Existe = $Departamentos->verificar($_POST['Departamento']);
    if(count($Existe) > 0){
        $Resultado = "Departamento existente.";
    }
    else{
        $Resultado = $Departamentos->insertar($_POST['Departamento']);
    }
}
?>

<script lang="javascript">
  if('<?php echo ((isset($Resultado)) ? $Resultado : "") ?>' != ""){
      alert('<?php echo ((isset($Resultado)) ? $Resultado : "")?>');
  }
</script>
<h1>Administrador departamentos</h1>
<div class="col-xs-4">
    <form class="form-horizontal" action="../administracion/departamentos.php" method="post">
        <input type="hidden" name="Crear" value="SI">
        <legend>Creaci&oacute;n de departamentos</legend>
        <div class="form-group">
            <label class="col-xs-4" for="">Departamento</label>
            <div class="col-xs-8">
                <input type="text" class="form-control QuitarEspeciales" id="Departamento" name="Departamento" placeholder="" required>
            </div>
        </div>
        <button type="submit" class="btn btn-success btn-block">
            Crear
        </button>

    </form>
</div>
<div class="col-xs-8">
    <h2>Listado de Departamentos</h2>
    <table class="table table-table-bordered table-hover">
    <thead>
        <tr>
            <th>Departamento</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $ListadoDepartamentos = $Departamentos->consultar();
            foreach ($ListadoDepartamentos as $Departamentos)
            {
                echo '<tr>
                <td>'.$Departamentos["descripcion"].'</td>
                </tr>';
            }
        ?>
    </tbody>
    </table>
</div>
<?php
include_once RUTA_RAIZ.'layout/footer.php';
