<?php
include_once '../global/configuracion.php';
session_start();
include_once RUTA_RAIZ.'layout/header.php';

include_once RUTA_RAIZ.'model/Perfiles.php';
$Perfiles = new Perfiles();
include_once RUTA_RAIZ.'model/Departamentos.php';
$Departamentos = new Departamentos();
include_once RUTA_RAIZ.'model/Funcionarios.php';
$Funcionarios = new Funcionarios();
include_once RUTA_RAIZ.'model/mod_lugares.php';
$Lugares = new Lugares();

if(isset($_POST) and isset($_POST["Crear"]) and $_POST["Crear"] == "SI"){
    # Se crea nuevo usuario
    $Existe = $Funcionarios->consultar(NULL, $_POST['Usuario']);
    if(count($Existe) > 0){
        $Resultado = "Usuario existente.";
    }
    else{
        $Resultado = $Funcionarios->insertar($_POST['Nombres'], $_POST['Apellidos'], $_POST['Usuario'], $_POST['Contrasena'], $_POST['Perfil'], $_POST['Departamento'],$_POST['Lugar']);
    }
}
?>
<script lang="javascript">
  if('<?php echo ((isset($Resultado)) ? $Resultado : "") ?>' != ""){
      alert('<?php echo ((isset($Resultado)) ? $Resultado : "")?>');
  }
</script>
<h1>Administrador de usuarios</h1>
<legend>Formulario creación de usuarios</legend>
<div class="row">
    <div class="col-xs-12">
      <form class="form-horizontal" action="../administracion/usuarios.php" method="post">
        <div class="col-xs-6">
            <input type="hidden" name="Crear" value="SI">
            <div class="form-group">
              <label class="col-xs-3" for="">Nombres</label>
              <div class="col-xs-9">
                <input type="text" class="form-control QuitarEspeciales" id="Nombres" name="Nombres" placeholder="" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-3" for="">Apellidos</label>
              <div class="col-xs-9">
                <input type="text" class="form-control QuitarEspeciales" id="Apellidos" name="Apellidos" placeholder="" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-3" for="">Usuario</label>
              <div class="col-xs-9">
                <input type="text" class="form-control QuitarEspeciales" id="Usuario" name="Usuario" placeholder="" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-3" for="">Contraseña</label>
              <div class="col-xs-9">
                <input type="password" class="form-control" id="Contrasena" name="Contrasena" placeholder="" required>
              </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
              <label class="col-xs-3" for="">Perfil</label>
              <div class="col-xs-9">
                <select name="Perfil" id="Perfil" class="form-control" required>
                  <option></option>
                  <?php
                  $ListadoPerfiles = $Perfiles->consultar();
                  foreach ($ListadoPerfiles as $Perfil) {
                    echo '<option value="'.$Perfil["id_perfil"].'">'.$Perfil["descripcion"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-3" for="">Departamento</label>
              <div class="col-xs-9">
                <select name="Departamento" id="Departamento" class="form-control" required>
                  <option></option>
                  <?php
                  $ListadoDepartamentos = $Departamentos->consultar();
                  foreach ($ListadoDepartamentos as $Departamento) {
                    echo '<option value="'.$Departamento["id_departamento"].'">'.$Departamento["descripcion"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-3" for="">Lugares</label>
              <div class="col-xs-9">
                <select name="Lugar" id="Lugar" class="form-control" required>
                  <option></option>
                  <?php
                  $ListadoLugares = $Lugares->consultar();
                  foreach ($ListadoLugares as $Lugar) {
                    echo '<option value="'.$Lugar["id_lugar"].'">'.$Lugar["depto"].'  - '.$Lugar["ciudad"].' - '.$Lugar["lugar"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <button type="submit" class="btn btn-success btn-block">
              Crear
            </button>
          </div>
      </form>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <h2>Listado de usuarios</h2>
        <table class="table table-table-bordered table-hover">
            <thead>
                <tr>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Usuario</th>
                    <th>Perfil</th>
                    <th>Departamento</th>
                    <th>Lugar</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $ListadoFuncionarios = $Funcionarios->consultar();
                foreach ($ListadoFuncionarios as $Funcionario) {
                echo '<tr>
                  <td>'.$Funcionario["nombres"].'</td>
                  <td>'.$Funcionario["apellidos"].'</td>
                  <td>'.$Funcionario["usuario"].'</td>
                  <td>'.$Funcionario["perfil"].'</td>
                  <td>'.$Funcionario["departamento"].'</td>
                  <td>'.$Funcionario["lugar"].'</td>
                </tr>';
                }
                ?>
            </tbody>
        </table>
    </div>    
</div>
<?php
include_once RUTA_RAIZ.'layout/footer.php';
