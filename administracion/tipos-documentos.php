<?php
include_once '../global/configuracion.php';
session_start();
$usr = $_SESSION['Usuario'];
// print_r($usr);
// echo implode(',',$usr);
include_once RUTA_RAIZ.'layout/header.php';
include_once RUTA_RAIZ.'model/TiposDocumentos.php';
$TiposDocumentos = new TiposDocumentos();

if(isset($_POST) and isset($_POST["Crear"]) and $_POST["Crear"] == "SI"){
    # Se crea nuevo usuario
    $Existe = $TiposDocumentos->verificar($_POST['Documento']);
    if(count($Existe) > 0){
        $Resultado = "Tipo Documento existente.";
    }
    else{
        $Resultado = $TiposDocumentos->insertar($_POST['Documento'],$usr['id_funcionario']);
    }
}
?>

<script lang="javascript">
  if('<?php echo ((isset($Resultado)) ? $Resultado : "") ?>' != ""){
      alert('<?php echo ((isset($Resultado)) ? $Resultado : "")?>');
  }
</script>
<h1>Administrador tipos de documentos</h1>
<div class="col-xs-4">
    <form class="form-horizontal" action="../administracion/tipos-documentos.php" method="post">
        <input type="hidden" name="Crear" value="SI">
        <legend>Creaci&oacute;n tipos documentos</legend>
        <div class="form-group">
            <label class="col-xs-4" for="">Documento:</label>
            <div class="col-xs-8">
                <input type="text" class="form-control QuitarEspeciales" id="Documento" name="Documento" placeholder="" required>
            </div>
        </div>
        <button type="submit" class="btn btn-success btn-block">
            Crear
        </button>

    </form>
</div>
<div class="col-xs-8">
    <h2>Listado de Tipos Documentos</h2>
    <table class="table table-table-bordered table-hover">
    <thead>
        <tr>
            <th>Tipo Documento</th>
            <th>Usuario Creacion</th>
            <th>Fecha Creacion</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $ListadoTiposDocumentos = $TiposDocumentos->consultar();
            foreach ($ListadoTiposDocumentos as $TiposDocumentos)
            {
                echo '<tr>
                <td>'.$TiposDocumentos["descripcion"].'</td>
                <td>'.$TiposDocumentos["usuario"].'</td>
                <td>'.$TiposDocumentos["fecha_registro"].'</td>
                </tr>';
            }
        ?>
    </tbody>
    </table>
</div>
<?php
include_once RUTA_RAIZ.'layout/footer.php';
