<?php
include_once '../global/configuracion.php';
session_start();
include_once RUTA_RAIZ.'layout/header.php';

include_once RUTA_RAIZ.'model/mod_depto_x_pais.php';
$Depto_pais = new DeptoPais();

include_once RUTA_RAIZ.'model/mod_ciudades.php';
$Ciudades = new Ciudades();

if(isset($_POST) and isset($_POST["Crear"]) and $_POST["Crear"] == "SI"){
    # Se crea nuevo usuario
    $Existe = $Ciudades->verificar($POST['id_depto'], $_POST['ciudad']);
    if(count($Existe) > 0){
        $Resultado = "Ciudad existente.";
    }
    else{
        $Resultado = $Ciudades->insertar($_POST['id_depto'], $_POST['ciudad']);
    }
}
?>
<script lang="javascript">
  if('<?php echo ((isset($Resultado)) ? $Resultado : "") ?>' != ""){
      alert('<?php echo ((isset($Resultado)) ? $Resultado : "")?>');
  }
</script>
<h1>Administrador de Ciudades</h1>
<div class="col-xs-4">
  <form class="form-horizontal" action="../administracion/ciudades.php" method="post">
    <input type="hidden" name="Crear" value="SI">
    <legend>Formulario creación ciudades</legend>
    <div class="form-group">
      <label class="col-xs-3" for="">Departamento</label>
      <div class="col-xs-9">
        <select name="id_depto" id="id_depto" class="form-control" required>
          <option></option>
          <?php
          $ListadoDepto = $Depto_pais->consultar();
          foreach ($ListadoDepto as $Depto) {
            echo '<option value="'.$Depto["id_depto"].'">'.$Depto["pais"].' - '.$Depto["depto"].'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-xs-3" for="">Ciudad</label>
      <div class="col-xs-9">
        <input type="text" class="form-control QuitarEspeciales" id="ciudad" name="ciudad" placeholder="" required>
      </div>
    </div>
    <button type="submit" class="btn btn-success btn-block">
      Crear
    </button>
  </form>
</div>
<div class="col-xs-8">
  <h2>Listado de Ciudad</h2>
  <table class="table table-table-bordered table-hover">
    <thead>
      <tr>
        <th>Pais</th>
        <th>Departamento</th>
        <th>Ciudad</th>
      </tr>
    </thead>
    <tbody>
  <?php
  $ListadoCiudades = $Ciudades->consultar();
  foreach ($ListadoCiudades as $Ciudad) {
    echo '<tr>
      <td>'.$Ciudad["pais"].'</td>
      <td>'.$Ciudad["depto"].'</td>
      <td>'.$Ciudad["ciudad"].'</td>
    </tr>';
  }
  ?>
  </tbody>
</table>
</div>
<?php
include_once RUTA_RAIZ.'layout/footer.php';
