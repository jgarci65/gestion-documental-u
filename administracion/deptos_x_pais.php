<?php
include_once '../global/configuracion.php';
session_start();
include_once RUTA_RAIZ.'layout/header.php';

include_once RUTA_RAIZ.'model/mod_paises.php';
$Paises = new Paises();
include_once RUTA_RAIZ.'model/mod_depto_x_pais.php';
$Depto_pais = new DeptoPais();

if(isset($_POST) and isset($_POST["Crear"]) and $_POST["Crear"] == "SI"){
    # Se crea nuevo usuario
    $Existe = $Depto_pais->verificar($POST['id_pais'], $_POST['depto']);
    if(count($Existe) > 0){
        $Resultado = "Departamento existente.";
    }
    else{
        $Resultado = $Depto_pais->insertar($_POST['id_pais'], $_POST['depto']);
    }
}
?>
<script lang="javascript">
  if('<?php echo ((isset($Resultado)) ? $Resultado : "") ?>' != ""){
      alert('<?php echo ((isset($Resultado)) ? $Resultado : "")?>');
  }
</script>
<h1>Administrador de Departamentos por pais</h1>
<div class="col-xs-4">
  <form class="form-horizontal" action="../administracion/deptos_x_pais.php" method="post">
    <input type="hidden" name="Crear" value="SI">
    <legend>Formulario creación departamentos</legend>
    <div class="form-group">
      <label class="col-xs-3" for="">Pais</label>
      <div class="col-xs-9">
        <select name="id_pais" id="id_pais" class="form-control" required>
          <option></option>
          <?php
          $ListadoPaises = $Paises->consultar();
          foreach ($ListadoPaises as $Pais) {
            echo '<option value="'.$Pais["id"].'">'.$Pais["descripcion"].'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-xs-3" for="">Departamento</label>
      <div class="col-xs-9">
        <input type="text" class="form-control QuitarEspeciales" id="depto" name="depto" placeholder="" required>
      </div>
    </div>
    <button type="submit" class="btn btn-success btn-block">
      Crear
    </button>
  </form>
</div>
<div class="col-xs-8">
  <h2>Listado de Departamentos por Pais</h2>
  <table class="table table-table-bordered table-hover">
    <thead>
      <tr>
        <th>Pais</th>
        <th>Departamento</th>
      </tr>
    </thead>
    <tbody>
  <?php
  $ListadoDepto_pais = $Depto_pais->consultar();
  foreach ($ListadoDepto_pais as $Depto_paises) {
    echo '<tr>
      <td>'.$Depto_paises["pais"].'</td>
      <td>'.$Depto_paises["depto"].'</td>
    </tr>';
  }
  ?>
  </tbody>
</table>
</div>
<?php
include_once RUTA_RAIZ.'layout/footer.php';
