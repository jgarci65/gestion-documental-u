<?php
include_once '../global/configuracion.php';
session_start();
include_once RUTA_RAIZ.'layout/header.php';

include_once RUTA_RAIZ.'model/mod_ciudades.php';
$Ciudades = new Ciudades();

include_once RUTA_RAIZ.'model/mod_lugares.php';
$Lugares = new Lugares();

if(isset($_POST) and isset($_POST["Crear"]) and $_POST["Crear"] == "SI"){
    # Se crea nuevo usuario
    $Existe = $Lugares->verificar($POST['id_ciudad'], $_POST['lugar']);
    if(count($Existe) > 0){
        $Resultado = "Ciudad existente.";
    }
    else{
        $Resultado = $Lugares->insertar($_POST['id_ciudad'], $_POST['lugar']);
    }
}
?>
<script lang="javascript">
  if('<?php echo ((isset($Resultado)) ? $Resultado : "") ?>' != ""){
      alert('<?php echo ((isset($Resultado)) ? $Resultado : "")?>');
  }
</script>
<h1>Administrador de Lugares</h1>
<div class="col-xs-4">
  <form class="form-horizontal" action="../administracion/lugares.php" method="post">
    <input type="hidden" name="Crear" value="SI">
    <legend>Formulario creación lugares</legend>
    <div class="form-group">
      <label class="col-xs-3" for="">Ciudad</label>
      <div class="col-xs-9">
        <select name="id_ciudad" id="id_ciudad" class="form-control" required>
          <option></option>
          <?php
          $ListadoCiudades = $Ciudades->consultar();
          foreach ($ListadoCiudades as $ciudad) {
            echo '<option value="'.$ciudad["id_ciudad"].'">'.$ciudad["pais"].' - '.$ciudad["depto"].' - '.$ciudad["ciudad"].'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-xs-3" for="">Lugar</label>
      <div class="col-xs-9">
        <input type="text" class="form-control QuitarEspeciales" id="lugar" name="lugar" placeholder="" required>
      </div>
    </div>
    <button type="submit" class="btn btn-success btn-block">
      Crear
    </button>
  </form>
</div>
<div class="col-xs-8">
  <h2>Listado de Lugares</h2>
  <table class="table table-table-bordered table-hover">
    <thead>
      <tr>
        <th>Pais</th>
        <th>Departamento</th>
        <th>Ciudad</th>
        <th>Lugar</th>
      </tr>
    </thead>
    <tbody>
  <?php
  $ListadoLugares = $Lugares->consultar();
  foreach ($ListadoLugares as $Lugar) {
    echo '<tr>
      <td>'.$Lugar["pais"].'</td>
      <td>'.$Lugar["depto"].'</td>
      <td>'.$Lugar["ciudad"].'</td>
      <td>'.$Lugar["lugar"].'</td>
    </tr>';
  }
  ?>
  </tbody>
</table>
</div>
<?php
include_once RUTA_RAIZ.'layout/footer.php';
