<?php
include_once '../global/configuracion.php';
session_start();
include_once RUTA_RAIZ.'layout/header.php';

include_once RUTA_RAIZ.'model/mod_paises.php';
$pais = new Paises();

if(isset($_POST) and isset($_POST["Crear"]) and $_POST["Crear"] == "SI"){
    # Se crea nuevo usuario
    $Existe = $pais->verificar($_POST['pais']);
    if(count($Existe) > 0){
        $Resultado = "Pais existente.";
    }
    else{
        $Resultado = $pais->insertar($_POST['pais']);
    }
}
?>

<script lang="javascript">
  if('<?php echo ((isset($Resultado)) ? $Resultado : "") ?>' != ""){
      alert('<?php echo ((isset($Resultado)) ? $Resultado : "")?>');
  }
</script>
<h1>Administrador Paises</h1>
<div class="col-xs-4">
    <form class="form-horizontal" action="../administracion/paises.php" method="post">
        <input type="hidden" name="Crear" value="SI">
        <legend>Creaci&oacute;n de paises</legend>
        <div class="form-group">
            <label class="col-xs-4" for="">Pais</label>
            <div class="col-xs-8">
                <input type="text" class="form-control QuitarEspeciales" id="pais" name="pais" placeholder="" required>
            </div>
        </div>
        <button type="submit" class="btn btn-success btn-block">
            Crear
        </button>

    </form>
</div>
<div class="col-xs-8">
    <h2>Listado de Paises</h2>
    <table class="table table-table-bordered table-hover">
    <thead>
        <tr>
            <th>Paises</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $ListadoPaises = $pais->consultar();
            foreach ($ListadoPaises as $paises)
            {
                echo '<tr>
                <td>'.$paises["descripcion"].'</td>
                </tr>';
            }
        ?>
    </tbody>
    </table>
</div>
<?php
include_once RUTA_RAIZ.'layout/footer.php';
