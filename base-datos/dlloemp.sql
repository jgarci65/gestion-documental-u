-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 09-05-2018 a las 15:37:23
-- Versión del servidor: 5.6.39-cll-lve
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `dlloemp`
--
CREATE DATABASE IF NOT EXISTS `dlloemp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dlloemp`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudades`
--

DROP TABLE IF EXISTS `ciudades`;
CREATE TABLE IF NOT EXISTS `ciudades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_depto` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

DROP TABLE IF EXISTS `departamentos`;
CREATE TABLE IF NOT EXISTS `departamentos` (
  `id_departamento` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) NOT NULL,
  PRIMARY KEY (`id_departamento`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos_pais`
--

DROP TABLE IF EXISTS `departamentos_pais`;
CREATE TABLE IF NOT EXISTS `departamentos_pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pais` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

DROP TABLE IF EXISTS `documentos`;
CREATE TABLE IF NOT EXISTS `documentos` (
  `id_documento` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) NOT NULL,
  `id_funcionario_registra` smallint(5) unsigned NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_documento`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos_radicado`
--

DROP TABLE IF EXISTS `documentos_radicado`;
CREATE TABLE IF NOT EXISTS `documentos_radicado` (
  `id_documentos_radicado` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_radicado` bigint(20) unsigned NOT NULL,
  `id_documento` bigint(20) unsigned NOT NULL,
  `cantidad` double unsigned NOT NULL,
  `id_funcionario_registra` smallint(5) unsigned NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_documentos_radicado`),
  KEY `idx_dr_id_radicado` (`id_radicado`),
  KEY `idx_dr_id_funcionario_registra` (`id_funcionario_registra`),
  KEY `idx_dr_id_documento` (`id_documento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionarios`
--

DROP TABLE IF EXISTS `funcionarios`;
CREATE TABLE IF NOT EXISTS `funcionarios` (
  `id_funcionario` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombres` varchar(250) NOT NULL,
  `apellidos` varchar(250) NOT NULL,
  `usuario` varchar(250) NOT NULL,
  `contrasena` varchar(250) NOT NULL,
  `id_perfil` tinyint(3) unsigned NOT NULL,
  `id_departamento` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id_funcionario`),
  UNIQUE KEY `uk_usuario` (`usuario`),
  KEY `idx_f_id_departamento` (`id_departamento`),
  KEY `idx_f_id_perfil` (`id_perfil`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugares`
--

DROP TABLE IF EXISTS `lugares`;
CREATE TABLE IF NOT EXISTS `lugares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ciudad` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

DROP TABLE IF EXISTS `paises`;
CREATE TABLE IF NOT EXISTS `paises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

DROP TABLE IF EXISTS `perfiles`;
CREATE TABLE IF NOT EXISTS `perfiles` (
  `id_perfil` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) NOT NULL,
  PRIMARY KEY (`id_perfil`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `radicados`
--

DROP TABLE IF EXISTS `radicados`;
CREATE TABLE IF NOT EXISTS `radicados` (
  `id_radicado` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_envia` varchar(250) NOT NULL,
  `correo_envia` varchar(250) NOT NULL,
  `id_funcionario_responsable` smallint(5) unsigned NOT NULL,
  `aceptado` enum('SI','NO') NOT NULL DEFAULT 'NO',
  `id_departamento` smallint(5) unsigned NOT NULL,
  `id_funcionario_radica` smallint(5) unsigned NOT NULL,
  `fecha_radica` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_radicado`),
  KEY `idx_r_id_funcionario_destino` (`id_funcionario_responsable`),
  KEY `idx_r_id_funcionario_radica` (`id_funcionario_radica`),
  KEY `idx_r_id_departamento` (`id_departamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traslados`
--

DROP TABLE IF EXISTS `traslados`;
CREATE TABLE IF NOT EXISTS `traslados` (
  `id_traslado` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_radicado` bigint(20) unsigned NOT NULL,
  `id_funcionario_destino` smallint(5) unsigned NOT NULL,
  `id_funcionario_origen` smallint(5) unsigned NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_traslado`),
  KEY `idx_t_id_radicado` (`id_radicado`),
  KEY `idx_t_id_funcionario_destino` (`id_funcionario_destino`),
  KEY `idx_t_id_funcionario_origen` (`id_funcionario_origen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `documentos_radicado`
--
ALTER TABLE `documentos_radicado`
  ADD CONSTRAINT `fk_dr_id_documento` FOREIGN KEY (`id_documento`) REFERENCES `documentos` (`id_documento`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dr_id_radicado` FOREIGN KEY (`id_radicado`) REFERENCES `radicados` (`id_radicado`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD CONSTRAINT `fk_f_id_departamento` FOREIGN KEY (`id_departamento`) REFERENCES `departamentos` (`id_departamento`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_f_id_perfil` FOREIGN KEY (`id_perfil`) REFERENCES `perfiles` (`id_perfil`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `traslados`
--
ALTER TABLE `traslados`
  ADD CONSTRAINT `fk_t_id_radicado` FOREIGN KEY (`id_radicado`) REFERENCES `radicados` (`id_radicado`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
