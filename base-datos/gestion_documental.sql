-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-03-2018 a las 18:59:27
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `gestion_documental`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE IF NOT EXISTS `departamentos` (
  `id_departamento` smallint(5) unsigned NOT NULL,
  `descripcion` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id_departamento`, `descripcion`) VALUES
(1, 'SISTEMAS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE IF NOT EXISTS `documentos` (
  `id_documento` bigint(20) unsigned NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `id_funcionario_registra` smallint(5) unsigned NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos_radicado`
--

CREATE TABLE IF NOT EXISTS `documentos_radicado` (
  `id_documentos_radicado` bigint(20) unsigned NOT NULL,
  `id_radicado` bigint(20) unsigned NOT NULL,
  `id_documento` bigint(20) unsigned NOT NULL,
  `cantidad` double unsigned NOT NULL,
  `id_funcionario_registra` smallint(5) unsigned NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionarios`
--

CREATE TABLE IF NOT EXISTS `funcionarios` (
  `id_funcionario` bigint(20) unsigned NOT NULL,
  `nombres` varchar(250) NOT NULL,
  `apellidos` varchar(250) NOT NULL,
  `usuario` varchar(250) NOT NULL,
  `contrasena` varchar(250) NOT NULL,
  `id_perfil` tinyint(3) unsigned NOT NULL,
  `id_departamento` smallint(5) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `funcionarios`
--

INSERT INTO `funcionarios` (`id_funcionario`, `nombres`, `apellidos`, `usuario`, `contrasena`, `id_perfil`, `id_departamento`) VALUES
(1, 'JAVIER', 'GARCIA', '1019027494', '8f14e45fceea167a5a36dedd4bea2543', 1, 1),
(2, 'MILENA', 'RUEDA', '5345834', '8f14e45fceea167a5a36dedd4bea2543', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

CREATE TABLE IF NOT EXISTS `perfiles` (
  `id_perfil` tinyint(3) unsigned NOT NULL,
  `descripcion` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`id_perfil`, `descripcion`) VALUES
(1, 'ADMINISTRADOR'),
(2, 'RADICADOR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `radicados`
--

CREATE TABLE IF NOT EXISTS `radicados` (
  `id_radicado` bigint(20) unsigned NOT NULL,
  `nombre_envia` varchar(250) NOT NULL,
  `correo_envia` varchar(250) NOT NULL,
  `id_funcionario_responsable` smallint(5) unsigned NOT NULL,
  `aceptado` enum('SI','NO') NOT NULL DEFAULT 'NO',
  `id_departamento` smallint(5) unsigned NOT NULL,
  `id_funcionario_radica` smallint(5) unsigned NOT NULL,
  `fecha_radica` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traslados`
--

CREATE TABLE IF NOT EXISTS `traslados` (
  `id_traslado` bigint(20) unsigned NOT NULL,
  `id_radicado` bigint(20) unsigned NOT NULL,
  `id_funcionario_destino` smallint(5) unsigned NOT NULL,
  `id_funcionario_origen` smallint(5) unsigned NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id_departamento`);

--
-- Indices de la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD PRIMARY KEY (`id_documento`);

--
-- Indices de la tabla `documentos_radicado`
--
ALTER TABLE `documentos_radicado`
  ADD PRIMARY KEY (`id_documentos_radicado`), ADD KEY `idx_dr_id_radicado` (`id_radicado`), ADD KEY `idx_dr_id_funcionario_registra` (`id_funcionario_registra`), ADD KEY `idx_dr_id_documento` (`id_documento`);

--
-- Indices de la tabla `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD PRIMARY KEY (`id_funcionario`), ADD UNIQUE KEY `uk_usuario` (`usuario`), ADD KEY `idx_f_id_departamento` (`id_departamento`), ADD KEY `idx_f_id_perfil` (`id_perfil`);

--
-- Indices de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  ADD PRIMARY KEY (`id_perfil`);

--
-- Indices de la tabla `radicados`
--
ALTER TABLE `radicados`
  ADD PRIMARY KEY (`id_radicado`), ADD KEY `idx_r_id_funcionario_destino` (`id_funcionario_responsable`), ADD KEY `idx_r_id_funcionario_radica` (`id_funcionario_radica`), ADD KEY `idx_r_id_departamento` (`id_departamento`);

--
-- Indices de la tabla `traslados`
--
ALTER TABLE `traslados`
  ADD PRIMARY KEY (`id_traslado`), ADD KEY `idx_t_id_radicado` (`id_radicado`), ADD KEY `idx_t_id_funcionario_destino` (`id_funcionario_destino`), ADD KEY `idx_t_id_funcionario_origen` (`id_funcionario_origen`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id_departamento` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `documentos`
--
ALTER TABLE `documentos`
  MODIFY `id_documento` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `documentos_radicado`
--
ALTER TABLE `documentos_radicado`
  MODIFY `id_documentos_radicado` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `funcionarios`
--
ALTER TABLE `funcionarios`
  MODIFY `id_funcionario` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  MODIFY `id_perfil` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `radicados`
--
ALTER TABLE `radicados`
  MODIFY `id_radicado` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `traslados`
--
ALTER TABLE `traslados`
  MODIFY `id_traslado` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `documentos_radicado`
--
ALTER TABLE `documentos_radicado`
ADD CONSTRAINT `fk_dr_id_documento` FOREIGN KEY (`id_documento`) REFERENCES `documentos` (`id_documento`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_dr_id_radicado` FOREIGN KEY (`id_radicado`) REFERENCES `radicados` (`id_radicado`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `funcionarios`
--
ALTER TABLE `funcionarios`
ADD CONSTRAINT `fk_f_id_departamento` FOREIGN KEY (`id_departamento`) REFERENCES `departamentos` (`id_departamento`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_f_id_perfil` FOREIGN KEY (`id_perfil`) REFERENCES `perfiles` (`id_perfil`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `traslados`
--
ALTER TABLE `traslados`
ADD CONSTRAINT `fk_t_id_radicado` FOREIGN KEY (`id_radicado`) REFERENCES `radicados` (`id_radicado`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
